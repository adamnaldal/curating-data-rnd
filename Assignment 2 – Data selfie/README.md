# Assignment 2 – Data Selfie
The final data visualisation can be seen [here](https://adamnaldal.github.io/curating-data-data-selfie/)

For privacy reasons the folder [Data and programs](https://gitlab.com/adamnaldal/curating-data-rnd/-/tree/master/Assignment%202%20%E2%80%93%20Data%20selfie/Data%20and%20programs) does not contain the original "payload.json" in which i received my data from tumblr.com. Instead it contains a version where i extracted only the data needed to generate the visualisation which i called "data_selfie_processed.json". It also contains the p5.js library that i used to write my visualisation script that generates the visualisation based on the "data_selfie_processed.json" file.
