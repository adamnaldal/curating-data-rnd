# Assignment 1 – Digitization
The folder [Data and Programs](https://gitlab.com/adamnaldal/curating-data-rnd/-/tree/master/Assignment%201%20%E2%80%93%20Digitization%20/Data%20and%20programs) contains the files i used in my work with the assignment, including the original readout from the sensor, the python script i used to create images, the final dataset created from these images by me subjectively assigning values to each of them.
It also contains all of the intermediary files i used/created in my work with getting the script to work and so on. 
