import pandas as pd
import os
import plotly.express as px
#import plotly.graph_objects as go

number = 0

if not os.path.exists("images"):
    os.mkdir("images")

df = pd.read_csv('bike_trip_1.csv',chunksize=800)

for chunk in df:
    fig = px.line(chunk, x="time", y="magnitude", title='Vibartion test 1')
    fig.write_image("images/fig%s.png" % number)
    number += 1
